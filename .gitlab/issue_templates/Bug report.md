## Summary

DESCRIPTION

## Environment

**GrayNLog version**: VERSION  
**TFM:** TFM  
**RID:** RID

## Steps to reproduce

The error occurs when the following initial conditions are met:

- CONDITION
- CONDITION
- CONDITION

The error occurs within following code block:

```c#
// YOUR CODE HERE
```

The error occurs as a result of the following steps:

1. STEP
1. STEP
1. STEP

## Expected behavior

WHAT DO YOU EXPECT?

## Actual behavior

WHAT IS ACTUAL RESULT?

The code throws following exception:

> EXCEPTION

The error produces following lines in internal log:

> LOG

/label ~"bug"