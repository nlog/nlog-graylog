﻿using GrayNLog.Targets;
using GrayNLog.Test.Simulation;
using GrayNLog.Test.TargetTests.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using System;
using System.IO;
using System.Text;

namespace GrayNLog.Test.TargetTests
{
    [TestClass]
    [TestCategory("UdpTarget")]
    public class UdpTargetTests : TargetTests<UdpTarget>
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public UdpTargetTests() : base("UDP")
        {
        }

        [TestMethod]
        [DynamicData(nameof(Samples), typeof(TargetTests<UdpTarget>), DynamicDataSourceType.Property)]
        public override void TestLogging(string nlogLevel,
                                         int sysLevel,
                                         Exception exception,
                                         string template,
                                         params EventProperty[] properties)
        {
            base.TestLogging(nlogLevel, sysLevel, exception, template, properties);
        }

        [TestMethod]
        [DoNotParallelize]
        public void TestChunking()
        {
            string fileContent = File.ReadAllText(@"Samples/The Queen of Spades.txt", Encoding.UTF8);
            string longMessage = new StringBuilder()
                .AppendLine("Compression and chunking might provide side effects. So it's necessary to test library's targets with long text message.")
                .AppendLine()
                .AppendLine(fileContent)
                .ToString();
            base.TestLogging("INFO", 6, null, longMessage);
        }

        protected override string SendMessage(LogLevel level, Exception exception, string template, params object[] arguments)
        {
            string actual = null;

            using (var input = new UdpInput(Port))
            {
                log.Log(level, exception, template, arguments);
                actual = input.Receive();
            }

            return actual;
        }
    }
}
