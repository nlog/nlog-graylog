using GrayNLog.Targets;
using GrayNLog.Test.Helpers;
using GrayNLog.Test.TargetTests.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace GrayNLog.Test.TargetTests
{
    /// <summary>
    /// Base class of a test fixture to test an NLog target sending GELF messages.
    /// </summary>
    /// <typeparam name="T">Type of tested target.</typeparam>
    /// <remarks>
    /// Follow the steps to create a test fixture for custom NLog target.
    /// <list type="number">
    /// <item>Inherit <see cref="TargetTests"/> class.</item>
    /// <item>Decorate derived class with <see cref="TestClassAttribute"/>.</item>
    /// <item>Define a <see cref="Logger"/> within derived class.</item>
    /// <item>Configure NLog to pass messages from the logger to the tested target.</item>
    /// <item>Pass tested target name to <see cref="TargetTests(string)"/> constructor.</item>
    /// <item>Implement <see cref="SendMessage(LogLevel, Exception, string, object[])"/> method.</item>
    /// <item>Decorate <see cref="TestLogging(string, int, Exception, string, EventProperty[])"/> method with <see cref="TestMethodAttribute"/>.</item>
    /// <item>Pass test cases data using <see cref="DataRowAttribute"/> or <see cref="DynamicDataAttribute"/>.</item>
    /// <item>Implement additional test methods.</item>
    /// </list>
    /// </remarks>
    [TestCategory("Target")]
    abstract public class TargetTests<T> where T : NLog.Targets.Target, INetworkTarget
    {
        private readonly string _targetName;

        /// <summary>
        /// Initialize an instance of test fixture.
        /// </summary>
        /// <param name="targetName">Name of NLog target to be tested.</param>
        protected TargetTests(string targetName)
        {
            _targetName = targetName;
        }

        /// <summary>
        /// Basic samples for <see cref="TestLogging(string, int, Exception, string, EventProperty[])"/> test.
        /// </summary>
        protected static IEnumerable<object[]> Samples
        {
            get
            {
                yield return new object[]
                {
                    "TRACE",
                    7,
                    null,
                    "Message with {inlineProperty}.",
                    new EventProperty
                    {
                        Name = "inlineProperty",
                        OriginalValue = null,
                        PropertyValue = null,
                        RenderedValue = "NULL"
                    }
                };
                yield return new object[]
                { 
                    "DEBUG",
                    7,
                    null,
                    "Message with {inlineProperty}.",
                    new EventProperty
                    {
                        Name = "inlineProperty",
                        OriginalValue = 0,
                        PropertyValue = 0,
                        RenderedValue = "0"
                    } 
                };
                yield return new object[]
                {
                    "INFO",
                    6,
                    null,
                    "Message with {inlineProperty}.",
                    new EventProperty
                    {
                        Name = "inlineProperty",
                        OriginalValue = "0",
                        PropertyValue = "0",
                        RenderedValue = "\"0\""
                    }
                };
                yield return new object[]
                {
                    "WARN",
                    4,
                    null,
                    "Message with {inlineProperty:l}.",
                    new EventProperty
                    {
                        Name = "inlineProperty",
                        OriginalValue = "0",
                        PropertyValue = "0",
                        RenderedValue = "0"
                    }
                };
                yield return new object[]
                {
                    "ERROR",
                    3,
                    new Exception("Error."),
                    "Message with {inlineProperty}.",
                    new EventProperty
                    {
                        Name = "inlineProperty",
                        OriginalValue = new { a = 0, b = 1 },
                        PropertyValue = "{ a = 0, b = 1 }",
                        RenderedValue = "{ a = 0, b = 1 }"
                    }
                };
                yield return new object[]
                {
                    "FATAL",
                    2,
                    new Exception("Fatal error."),
                    "Message with {inlineProperty}.",
                    new EventProperty
                    {
                        Name = "inlineProperty",
                        OriginalValue = true,
                        PropertyValue = true,
                        RenderedValue = "true"
                    }
                };
                yield return new object[] {
                    "INFO",
                    6,
                    null,
                    "Message with {inlineProperty}.",
                    new EventProperty
                    {
                    Name = "inlineProperty",
                    OriginalValue = new MyObject { A = 1, B = 2 },
                    PropertyValue = "1+2=3",
                    RenderedValue = "1+2=3"
                    }
                };
            }
        }

        /// <summary>
        /// Expected value of <c>host</c> field of a GELF message sent by tested target.
        /// </summary>
        protected string HostName
        {
            get
            {
                string name = Environment.GetEnvironmentVariable("COMPUTERNAME")?.Trim();
                if (!String.IsNullOrEmpty(name))
                    return name;
                name = Environment.GetEnvironmentVariable("HOSTNAME")?.Trim();
                if (!String.IsNullOrEmpty(name))
                    return name;
                name = Dns.GetHostName()?.Trim();
                if (!String.IsNullOrEmpty(name))
                    return name;
                return null;
            }
        }

        /// <summary>
        /// The port number to listen messages sent by the tested target.
        /// </summary>
        protected int Port
        {
            get
            {
                INetworkTarget target = LogManager.Configuration.FindTargetByName(_targetName) as T;
                return target.Port;
            }
        }

        /// <summary>
        /// Sends an event to the tested NLog target and check the GELF message sent by the target is expected.
        /// </summary>
        /// <param name="nlogLevel">Name of NLog level.</param>
        /// <param name="sysLevel">Syslog message severity corresponding with <paramref name="nlogLevel"/>.</param>
        /// <param name="exception">An exception to be logged.</param>
        /// <param name="template">A message string containing a format item.</param>
        /// <param name="properties">Event properties included to the log message.</param>
        /// <remarks>
        /// Override and decorate the method within derived class using <see cref="TestMethodAttribute"/>
        /// and call the base method from it.
        /// Pass test cases data using <see cref="DataRowAttribute"/> or <see cref="DynamicDataAttribute"/>.
        /// </remarks>
        public virtual void TestLogging(string nlogLevel,
                                        int sysLevel,
                                        Exception exception,
                                        string template,
                                        params EventProperty[] properties)
        {
            string message = template;
            foreach(EventProperty property in properties)
                message = Regex.Replace(message, $@"\{{[$@]?{property.Name}(\:\w)?\}}", property.RenderedValue);

            string callSite = $"{this.GetType().FullName}.{nameof(SendMessage)}";

            var builder = new JsonBuilder()
                .Add("version", "1.1")
                .Add("host", HostName)
                .Add("timestamp", DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() / 1000)
                .Add("short_message", message)
                .Add("full_message", $"{nlogLevel}: {message}")
                .Add("level", sysLevel)
                .Add("_contextCallSite", callSite)
                .Add("_contextProperty", "Context value")
                .Add("_exception", exception?.ToString() ?? "");

            foreach (EventProperty property in properties)
                builder.Add($"_{property.Name}", property.PropertyValue);

            string expected = builder.ToString();

            LogLevel level = LogLevel.AllLevels.FirstOrDefault(lev => String.Equals(lev.Name, nlogLevel, StringComparison.OrdinalIgnoreCase));

            object[] originalValues = properties.Select(p => p.OriginalValue).ToArray();
            string actual = SendMessage(level, exception, template, originalValues);

            if (actual == null)
                Assert.Fail("The message is not received.");
            Assert.IsTrue(MessageEqualityComparer.AreEqual(expected, actual), "Received message is unexpected.");
        }

        /// <summary>
        /// Sends a log event and receives a string representation of it from the tested target.
        /// </summary>
        /// <param name="level">The log level.</param>
        /// <param name="exception">An exception to be logged.</param>
        /// <param name="template">A string containing a format item.</param>
        /// <param name="arguments">The arguments to format.</param>
        /// <returns>String representation of GELF message sent by the tested target.</returns>
        /// <remarks>
        /// Use <see cref="Logger.Log(LogLevel, Exception, IFormatProvider, string, object[])"/>
        /// method of derived class logger to write message to the tested target.
        /// Then listen port number <see cref="Port"/>, receive the GELF message sent by the target
        /// and return string representation of it.
        /// </remarks>
        protected abstract string SendMessage(LogLevel level, Exception exception, string template, params object[] arguments);

        private class MyObject
        { 
            public int A { get; set; }
            public int B { get; set; }
            public override string ToString() => $"{A}+{B}={A+B}";
        }
    }
}
