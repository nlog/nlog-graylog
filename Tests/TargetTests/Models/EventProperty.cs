﻿namespace GrayNLog.Test.TargetTests.Models
{
    /// <summary>
    /// Model of an event property to test NLog targets.
    /// </summary>
    public class EventProperty
    {
        /// <summary>
        /// Property name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Original property value.
        /// </summary>
        public object OriginalValue { get; set; }

        /// <summary>
        /// The expected value with which the property must be replaced in rendered text.
        /// </summary>
        public string RenderedValue { get; set; }

        /// <summary>
        /// Expected GELF property value.
        /// </summary>
        public object PropertyValue { get; set; }
    }
}
