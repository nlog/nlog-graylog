﻿using System;
using System.Text;
using System.Text.Json;

namespace GrayNLog.Test.Helpers
{
    class JsonBuilder
    {
        private readonly StringBuilder _builder = new StringBuilder();
        private bool _closed = false;

        public JsonBuilder()
        {
            Clear();
        }

        public JsonBuilder Add(string property, object value)
        {
            if (property == null)
                throw new ArgumentNullException(nameof(property));
            if (_closed)
                _builder.Remove(_builder.Length - 1, 1);
            string json = JsonSerializer.Serialize(value);
            string before = _builder.Length == 0 ? "{" : ",";
            _builder.Append(before).Append("\"").Append(property).Append("\":").Append(json);
            _closed = false;
            return this;
        }

        public JsonBuilder Clear()
        {
            _builder.Clear();
            _closed = false;
            return this;
        }

        public override string ToString()
        {
            if (_builder.Length > 0 && !_closed)
                Close();
            return _builder.ToString();
        }

        private void Close()
        {
            _builder.Append("}");
            _closed = true;
        }
    }
}
