﻿using GrayNLog.Test.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace GrayNLog.Test.EqualityTests
{
    [TestClass]
    [TestCategory("Equality")]
    public class JsonEqualityTests
    {
        #region Counter
        public TestContext TestContext { get; set; }

        protected int Count
        {
            get
            {
                return (int)(TestContext.Properties["TestCaseNumber"] ?? 0);
            }
            private set
            {
                TestContext.Properties["TestCaseNumber"] = value;
            }
        }

        [TestInitialize]
        public virtual void SetupTest() => this.Count++;
        #endregion

        #region Data
        private static IEnumerable<string> Samples
        {
            get
            {
                yield return "null";
                yield return "true";
                yield return "false";
                yield return "1";
                yield return "2";
                yield return "\"a\"";
                yield return "\"A\"";
                yield return "[]";
                yield return "[1]";
                yield return "[2]";
                yield return "[1, 2]";
                yield return "{}";
                yield return "{ \"a\": null }";
                yield return "{ \"a\": true }";
                yield return "{ \"a\": false }";
                yield return "{ \"a\": \"a\" }";
                yield return "{ \"a\": \"A\" }";
                yield return "{ \"a\": [] }";
                yield return "{ \"a\": [1] }";
                yield return "{ \"a\": [2] }";
                yield return "{ \"a\": [1, 2] }";
                yield return "{ \"a\": {} }";
                yield return "{ \"A\": \"a\" }";
                yield return "{ \"a\": true, \"b\": null }";
            }
        }

        private static IEnumerable<object[]> Reflexivity
        {
            get => Samples.Select(s => new object[1] { s }).ToList();
        }

        public static IEnumerable<object[]> Symmetry
        {
            get
            {
                int i = 0;
                IEnumerable<string> samples = Samples.ToList();
                foreach (string x in samples)
                {
                    foreach (string y in samples.Skip(i++))
                        yield return new object[2] { x, y };
                }
                samples = null;
                yield break;
            }
        }

        public static IEnumerable<object[]> Transitivity
        {
            get => Samples.Select(s => new object[3] { s, s, s }).ToList();
        }

        public static IEnumerable<object[]> Equal
        {
            get => Samples.Select(s => new object[2] { s, s }).ToList();
        }

        public static IEnumerable<object[]> Inequal
        {
            get
            {
                int i = 0;
                IEnumerable<string> samples = Samples.ToList();
                foreach (string x in samples)
                {
                    foreach (string y in samples.Skip(++i))
                        yield return new object[2] { x, y };
                }
                samples = null;
                yield break;
            }
        }
        #endregion

        #region Test Methods
        [TestMethod]
        [DynamicData(nameof(Reflexivity))]
        public virtual void TestReflexivity(string x)
        {
            Assert.AreEqual(MessageEqualityComparer.GetHashCode(x), MessageEqualityComparer.GetHashCode(x), $"Hash function doesn't consistently return the same hash code for sample {this.Count}.");
            Assert.IsTrue(MessageEqualityComparer.Equals(x, x), $"The Equals method is not reflexive for sample {this.Count}.");
        }

        [TestMethod]
        [DynamicData(nameof(Symmetry))]
        public virtual void TestSymmetry(string x, string y)
        {
            Assert.AreEqual(MessageEqualityComparer.Equals(x, y), MessageEqualityComparer.Equals(y, x), $"The Equals method is not symmetric for data set {this.Count}.");
        }

        [TestMethod]
        [DynamicData(nameof(Transitivity))]
        public virtual void TestTransitivity(string x, string y, string z)
        {
            if (MessageEqualityComparer.Equals(x, y) && MessageEqualityComparer.Equals(y, z))
            {
                Assert.IsTrue(MessageEqualityComparer.Equals(x, z), $"The Equals method is not transitive for data set {this.Count}.");
            }
        }

        [TestMethod]
        [DynamicData(nameof(Equal))]
        public virtual void TestEqual(string x, string y)
        {
            Assert.AreEqual(MessageEqualityComparer.GetHashCode(x), MessageEqualityComparer.GetHashCode(y), $"Hash function returns different hash codes for equal items of data set {this.Count}.");
            Assert.IsTrue(MessageEqualityComparer.Equals(x, y), $"Equals method doesn't detect equality for equal items of data set {this.Count}.");
        }

        [TestMethod]
        [DynamicData(nameof(Inequal))]
        public virtual void TestUnequal(string x, string y)
        {
            Assert.IsFalse(MessageEqualityComparer.Equals(x, y), $"Equals method considers unequal items of set {this.Count} as equal.");
        }
        #endregion
    }
}
