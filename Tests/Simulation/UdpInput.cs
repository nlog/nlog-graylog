﻿using GrayNLog.Test.Helpers;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace GrayNLog.Test.Simulation
{
    /// <summary>
    /// Graylog UDP Input simulator.
    /// </summary>
    class UdpInput : IDisposable
    {
        private readonly UdpClient _client;

        /// <summary>
        /// Initializes an instance of the simulator.
        /// </summary>
        /// <param name="port">Local port number to listen to.</param>
        public UdpInput(int port)
        {
            _client = new UdpClient(port);
        }

        /// <summary>
        /// Receive data from the listening port.
        /// </summary>
        /// <returns>
        /// Received message or <see langword="null"/> if there is no available data on the listened port.
        /// </returns>
        public string Receive()
        {
            byte[] raw = ReadAll();
            if (raw == null)
                return null;
            if (raw.Length == 0)
                return String.Empty;
            byte[] data = GZipHelper.IsCompressed(raw)
                ? GZipHelper.Decompress(raw)
                : raw;
            return Encoding.UTF8.GetString(data);
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            _client?.Dispose();
        }

        private byte[] ReadAll()
        {
            byte[][] chunks = null;
            IPEndPoint endpoint = new IPEndPoint(IPAddress.Loopback, 0);
            while (_client.Available > 0)
            {
                byte[] chunk = _client.Receive(ref endpoint);
                if (chunk.Length >= 12 && chunk[0] == 0x1e && chunk[1] == 0x0f)
                {
                    chunks ??= new byte[chunk[11]][];
                    chunks[chunk[10]] = chunk.Skip(12).ToArray();
                }
                else
                    return chunk;
            }
            byte[] data = chunks?.SelectMany(b => b ?? Enumerable.Empty<byte>())?.ToArray();
            return data;
        }
    }
}
