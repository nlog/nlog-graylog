﻿using NLog;
using NLog.Layouts;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace GrayNLog.Targets
{
    /// <summary>
    /// Provides auxiliary methods to use within NLog targets implemented by the library.
    /// </summary>
    internal static class TargetHelper
    {
        private static readonly IDictionary<LogLevel, SyslogLevel> _levelMap;
        private static readonly Func<TargetWithContext, Layout, LogEventInfo, string> _render;
        private static readonly Func<TargetWithContext, LogEventInfo, IDictionary<string, object>> _getProperties;
        private static string _host;

        static TargetHelper()
        {
            Type targetType = typeof(TargetWithContext);

            MethodInfo renderMethod = targetType.GetMethod("RenderLogEvent", BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(Layout), typeof(LogEventInfo) }, null);
            _render = (Func<TargetWithContext, Layout, LogEventInfo, string>)Delegate.CreateDelegate(typeof(Func<TargetWithContext, Layout, LogEventInfo, string>), renderMethod, true);

            MethodInfo getAllPropertiesMethod = targetType.GetMethod("GetAllProperties", BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(LogEventInfo) }, null);
            _getProperties = (Func<TargetWithContext, LogEventInfo, IDictionary<string, object>>)Delegate.CreateDelegate(typeof(Func<TargetWithContext, LogEventInfo, IDictionary<string, object>>), getAllPropertiesMethod, true);

            _levelMap = new Dictionary<LogLevel, SyslogLevel>
            {
                [LogLevel.Trace] = SyslogLevel.Debug,
                [LogLevel.Debug] = SyslogLevel.Debug,
                [LogLevel.Info] = SyslogLevel.Informational,
                [LogLevel.Warn] = SyslogLevel.Warning,
                [LogLevel.Error] = SyslogLevel.Error,
                [LogLevel.Fatal] = SyslogLevel.Critical
            };
        }

        /// <summary>
        /// Creates a dictionary containing GELF properties for specified event.
        /// </summary>
        /// <param name="target">NLog target to be extended.</param>
        /// <param name="logEvent">Logging event to extract properties.</param>
        /// <returns>
        /// A dictionary containing GELF properties for <paramref name="logEvent"/>.
        /// </returns>
        public static IDictionary<string, object> GetGelfProperties(this TargetWithContext target, LogEventInfo logEvent)
        {
            if (target == null)
                throw new ArgumentNullException(nameof(target));
            if (logEvent == null)
                throw new ArgumentNullException(nameof(logEvent));

            string host = GetHostName(target, logEvent);
            string fullMessage = _render(target, target.Layout, logEvent);
            IEnumerable<KeyValuePair<string, object>> eventSnapshot = _getProperties(target, logEvent)?.ToList();

            IDictionary<string, object> properties = new Dictionary<string, object>
            {
                ["version"] = "1.1",
                ["host"] = host,
                ["short_message"] = logEvent.FormattedMessage,
                ["timestamp"] = logEvent.GetTimestamp(),
                ["level"] = logEvent.Level.ToGelfLevel()
            };

            if (!String.IsNullOrEmpty(fullMessage))
                properties.Add("full_message", fullMessage);

            if (eventSnapshot == null)
                return properties;

            foreach (KeyValuePair<string, object> property in eventSnapshot)
            {
                string name = $"_{property.Key ?? String.Empty}";
                properties[name] = Convert.GetTypeCode(property.Value) == TypeCode.Object
                    ? property.Value?.ToString()
                    : property.Value;
            }

            return properties;
        }

        private static string GetHostName(TargetWithContext target, LogEventInfo logEvent)
        {
            if(_host == null)
                _host = _render(target, "${hostname}", logEvent);

            return _host;
        }

        private static double GetTimestamp(this LogEventInfo logEvent)
        {
            long totalMilliseconds = logEvent == null
                ? DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()
                : new DateTimeOffset(logEvent.TimeStamp).ToUnixTimeMilliseconds();
            return totalMilliseconds / 1000d;
        }

        private static byte ToGelfLevel(this LogLevel level) => (byte)_levelMap[level];
    }
}
