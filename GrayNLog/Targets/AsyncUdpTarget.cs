﻿using GrayNLog.Transport;
using NLog;
using NLog.Common;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace GrayNLog.Targets
{
    /// <summary>
    /// NLog target to pass logging events to the Graylog via UDP asynchronously.
    /// </summary>
    /// <seealso href="https://docs.graylog.org/en/4.0/pages/gelf.html#gelf-via-udp">GELF via UDP</seealso>
    [Target("AsyncUdpGelf")]
    public class AsyncUdpTarget : AsyncTaskTarget, INetworkTarget
    {
        private UdpClient _client;

        /// <summary>
        /// Initializes an instance of the target.
        /// </summary>
        public AsyncUdpTarget()
            : base()
        {
            IncludeEventProperties = true;
            Port = 12201;
            CompressionThreshold = 512;
        }

        /// <inheritdoc/>
        [RequiredParameter]
        public string Host { get; set; }

        /// <inheritdoc/>
        public int Port { get; set; }

        /// <summary>
        /// Message size, exceeding which the compression effect is engaged, bytes.
        /// </summary>
        /// <remarks>
        /// The default value is 512 bytes.
        /// Use negative values to disable compression.
        /// </remarks>
        public int CompressionThreshold { get; set; }

        /// <inheritdoc/>
        protected override void InitializeTarget()
        {
            base.InitializeTarget();
            _client = new UdpClient(Host, Port)
            {
                CompressionThreshold = this.CompressionThreshold
            };
            InternalLogger.Debug("{0}[{1}]. Initialized for {3}:{4}.", this.GetType(), Name, Host, Port);
        }

        /// <inheritdoc/>
        protected override void Dispose(bool disposing)
        {
            _client?.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// Writes logging event to the log target asynchronously.
        /// </summary>
        /// <param name="logEvent">Logging event.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns>
        /// Asynchronously operation of the logging event writing.
        /// </returns>
        protected override async Task WriteAsyncTask(LogEventInfo logEvent, CancellationToken cancellationToken)
        {
            try
            {
                IDictionary<string, object> properties = this.GetGelfProperties(logEvent);
                byte[] message = null;
                using (var stream = new MemoryStream())
                {
                    await JsonSerializer.SerializeAsync<IDictionary<string, object>>(stream, properties, null, cancellationToken);
                    message = stream.ToArray();
                }
                bool success = await _client.SendAsync(message, cancellationToken);

                if (!success)
                    InternalLogger.Error("{0}[{1}]. Sending is not succeed. {2}.", GetType(), Name, logEvent);
            }
            catch (Exception) when (cancellationToken.IsCancellationRequested)
            {
                InternalLogger.Warn("{0}[{1}]. Sending is canceled. {2}.", GetType(), Name, logEvent);
            }
            catch (Exception error)
            {
                InternalLogger.Error(error, "{0}[{1}]. Failed to send {2}.", GetType(), Name, logEvent);
            }
        }
    }
}
